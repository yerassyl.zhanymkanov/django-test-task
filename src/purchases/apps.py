from django.apps import AppConfig


class PurchasesConfig(AppConfig):
    name = 'src.purchases'
    label = 'purchases'
