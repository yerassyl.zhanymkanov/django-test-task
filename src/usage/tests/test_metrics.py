import json

from django.urls import reverse
from rest_framework.test import APITestCase

from src.usage import exceptions


class MetricsTests(APITestCase):
    url = reverse("usage-metrics")

    def test_data_usage_ok(self):
        payload = {
            "from-date": "2020-05-01", "to-date": "2020-05-10", "usage-type": "data"
        }

        response = self.client.get(self.url, payload)
        self.assertEqual(response.status_code, 200)

    def test_voice_usage_ok(self):
        payload = {
            "from-date": "2020-05-01", "to-date": "2020-05-10", "usage-type": "voice"
        }

        response = self.client.get(self.url, payload)
        self.assertEqual(response.status_code, 200)

    def test_dates_required(self):
        payload = {
            "usage-type": "data"
        }

        response = self.client.get(self.url, payload)
        response_json = json.loads(response.content)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response_json["detail"], exceptions.DatesRequired.default_detail
        )

    def test_usage_type_required(self):
        payload = {
            "from-date": "2020-05-01", "to-date": "2020-05-10"
        }

        response = self.client.get(self.url, payload)
        response_json = json.loads(response.content)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response_json["detail"], exceptions.UsageTypeRequired.default_detail
        )

    def test_usage_type_invalid(self):
        payload = {
            "from-date": "2020-05-01", "to-date": "2020-05-10", "usage-type": "INVALID"
        }

        response = self.client.get(self.url, payload)
        response_json = json.loads(response.content)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response_json["detail"], exceptions.InvalidUsageType.default_detail
        )
