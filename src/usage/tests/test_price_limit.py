import json

from django.urls import reverse
from rest_framework.test import APITestCase

from src.usage import exceptions


class PriceLimitTests(APITestCase):
    url = reverse("price-limit")

    def test_price_limit_ok(self):
        payload = {"price-limit": 100}

        response = self.client.get(self.url, payload)
        self.assertEqual(response.status_code, 200)

    def test_price_limit_required(self):
        payload = {"invalid_price_limit": 100}

        response = self.client.get(self.url, payload)
        response_json = json.loads(response.content)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response_json["detail"],
            exceptions.PriceLimitRequired.default_detail
        )

    def test_price_limit_invalid(self):
        payload = {"price-limit": "STRING"}

        response = self.client.get(self.url, payload)
        response_json = json.loads(response.content)

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response_json["detail"],
            exceptions.InvalidPriceLimit.default_detail
        )
