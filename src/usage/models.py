from django.db import models

from src.att_subscriptions.models import ATTSubscription
from src.sprint_subscriptions.models import SprintSubscription
from src.usage.managers import AggregatesManager


class BaseUsageRecord(models.Model):
    att_subscription_id = models.ForeignKey(
        ATTSubscription, null=True, on_delete=models.PROTECT
    )
    sprint_subscription_id = models.ForeignKey(
        SprintSubscription, null=True, on_delete=models.PROTECT
    )
    price = models.DecimalField(decimal_places=2, max_digits=5, default=0)
    usage_date = models.DateTimeField(null=False)

    class Meta:
        abstract = True


class DataUsageRecord(BaseUsageRecord):
    """Raw data usage record for a subscription"""
    kilobytes_used = models.IntegerField(null=False)


class VoiceUsageRecord(BaseUsageRecord):
    """Raw voice usage record for a subscription"""
    seconds_used = models.IntegerField(null=False)


class AggregatedUsage(models.Model):
    att_subscription = models.ForeignKey(
        ATTSubscription, null=True, on_delete=models.PROTECT
    )
    sprint_subscription = models.ForeignKey(
        SprintSubscription, null=True, on_delete=models.PROTECT
    )
    date = models.DateField(null=False)

    total_kilobytes = models.IntegerField(null=False)
    total_seconds = models.IntegerField(null=False)

    data_price = models.DecimalField(decimal_places=2, max_digits=5, default=0)
    voice_price = models.DecimalField(decimal_places=2, max_digits=5, default=0)

    objects = AggregatesManager()
