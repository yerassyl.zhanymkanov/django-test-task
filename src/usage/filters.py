from rest_framework import filters

from src.usage.exceptions import DatesRequired, InvalidDateFormat
from src.usage.utils import is_valid_date


class DatesRangeFilterBackend(filters.BaseFilterBackend):
    """
    Filter that only allows users to see their own objects.
    """
    def filter_queryset(self, request, queryset, view):
        from_date = request.query_params.get("from-date")
        to_date = request.query_params.get("to-date")

        if not from_date or not to_date:
            raise DatesRequired()

        dates_range = (from_date, to_date)
        if not all(is_valid_date(date) for date in dates_range):
            raise InvalidDateFormat()

        return queryset.filter(date__range=dates_range)
