from rest_framework.generics import ListAPIView

from .constants import VOICE_USAGE_TYPE, DATA_USAGE_TYPE
from .filters import DatesRangeFilterBackend
from .models import AggregatedUsage
from .serializers import (
    AggregatedUsageSerializer,
    DataMetricsSerializer,
    VoiceMetricsSerializer
)
from .utils import validate_usage_type, validate_price_limit


class SubscriptionPriceLimit(ListAPIView):
    serializer_class = AggregatedUsageSerializer

    def get_queryset(self):
        price_limit = self.request.query_params.get("price-limit")
        price_limit = validate_price_limit(price_limit)

        return AggregatedUsage.objects.price_limits(price_limit)


class SubscriptionUsageMetrics(ListAPIView):
    filter_backends = [DatesRangeFilterBackend]

    USAGE_TYPE_SERIALIZERS = {
        DATA_USAGE_TYPE: DataMetricsSerializer,
        VOICE_USAGE_TYPE: VoiceMetricsSerializer,
    }

    def get_serializer_class(self):
        usage_type = self.request.query_params.get("usage-type").lower()
        return self.USAGE_TYPE_SERIALIZERS[usage_type]  # value is already validated

    def get_queryset(self):
        usage_type = self.request.query_params.get("usage-type")
        usage_type = validate_usage_type(usage_type)

        return AggregatedUsage.objects.metrics(usage_type)
