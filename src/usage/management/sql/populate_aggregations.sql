INSERT INTO usage_aggregatedusage(att_subscription_id, sprint_subscription_id, date,
                                  data_price, voice_price, total_kilobytes,
                                  total_seconds)
SELECT ud.att_subscription_id_id    att_subscription_id,
       ud.sprint_subscription_id_id sprint_subscription_id,
       date(ud.usage_date)          date,
       sum(ud.price)                data_price,
       sum(uv.price)                voice_price,
       sum(ud.kilobytes_used)       total_kilobytes,
       sum(uv.seconds_used)         total_seconds
FROM usage_datausagerecord ud
         JOIN usage_voiceusagerecord uv
              ON ud.att_subscription_id_id = uv.att_subscription_id_id
                  OR ud.sprint_subscription_id_id = uv.sprint_subscription_id_id
GROUP BY date, att_subscription_id, sprint_subscription_id;