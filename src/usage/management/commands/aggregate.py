import logging

from django.core.management.base import BaseCommand
from django.db import connection

logger = logging.getLogger("populate_script")
logging.basicConfig(level=logging.DEBUG)


class Command(BaseCommand):
    help = 'Populate aggregation table with data'

    def read_sql(self):
        with open("src/usage/management/sql/populate_aggregations.sql") as f:
            return f.read()

    def handle(self, *args, **options):
        logger.info("Reading a SQL from file")
        populate_sql = self.read_sql()

        with connection.cursor() as cursor:
            logger.info("Executing a table population query")
            cursor.execute(populate_sql)
            logger.info("Aggregations table is successfully populated")
