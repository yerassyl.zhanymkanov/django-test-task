from django.core.management.base import BaseCommand

from ._populate import populate_db


class Command(BaseCommand):
    help = 'Populates entire database with data'

    def handle(self, *args, **options):
        populate_db()
