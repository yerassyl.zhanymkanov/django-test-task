import datetime
import logging
import random
from typing import List

from django.contrib.auth.models import User

from src.plans.models import Plan
from src.att_subscriptions.models import ATTSubscription
from src.sprint_subscriptions.models import SprintSubscription
from src.usage.models import VoiceUsageRecord, DataUsageRecord

from . import _utils as utils

logger = logging.getLogger("populate_script")
logging.basicConfig(level=logging.DEBUG)


def random_user():
    return User.objects.create_user(
        username=utils.random_text(),
        email=utils.random_email(),
        password=utils.random_text(),
    )


def populate_plans():
    return Plan.objects.bulk_create(
        Plan(
            name=utils.random_text(),
            price=utils.random_price(),
            data_available=utils.random_kilobytes()
        )
        for _ in range(10)
    )


def populate_att_subs(plans: List[Plan]):
    return ATTSubscription.objects.bulk_create(
        ATTSubscription(
            user=random_user(),
            plan=random.choice(plans),
            effective_date=datetime.date(2022, 12, 31)
        )
        for _ in range(10)
    )


def populate_sprint_subs(plans: List[Plan]):
    return SprintSubscription.objects.bulk_create(
        SprintSubscription(
            user=random_user(),
            plan=random.choice(plans),
            effective_date=datetime.date(2022, 12, 31)
        )
        for _ in range(10)
    )


def populate_usage(
    att_subs: List[ATTSubscription], sprint_subs: List[SprintSubscription]
):
    _populate_att_usage(att_subs)
    _populate_sprint_usage(sprint_subs)


def _populate_att_usage(att_subs):
    DataUsageRecord.objects.bulk_create(
        DataUsageRecord(
            att_subscription_id=random.choice(att_subs),
            price=utils.random_small_price(),
            usage_date=utils.random_datetime(),
            kilobytes_used=utils.random_used_kilobytes()
        )
        for _ in range(100)
    )
    VoiceUsageRecord.objects.bulk_create(
        VoiceUsageRecord(
            att_subscription_id=random.choice(att_subs),
            price=utils.random_small_price(),
            usage_date=utils.random_datetime(),
            seconds_used=utils.random_seconds()
        )
        for _ in range(100)
    )


def _populate_sprint_usage(sprint_subs):
    DataUsageRecord.objects.bulk_create(
        DataUsageRecord(
            sprint_subscription_id=random.choice(sprint_subs),
            price=utils.random_small_price(),
            usage_date=utils.random_datetime(),
            kilobytes_used=utils.random_used_kilobytes()
        )
        for _ in range(100)
    )
    VoiceUsageRecord.objects.bulk_create(
        VoiceUsageRecord(
            sprint_subscription_id=random.choice(sprint_subs),
            price=utils.random_small_price(),
            usage_date=utils.random_datetime(),
            seconds_used=utils.random_seconds()
        )
        for _ in range(100)
    )


def populate_db():
    logger.info("Started populating plans")
    sprint_plans = populate_plans()
    att_plans = populate_plans()
    logger.info("Finished populating plans")

    logger.info("Started populating subscriptions")
    sprint_subs = populate_sprint_subs(sprint_plans)
    att_subs = populate_att_subs(att_plans)
    logger.info("Finished populating subscriptions")

    logger.info("Started populating data usage")
    populate_usage(sprint_subs=sprint_subs[:10], att_subs=att_subs[:10])
    logger.info("Finished populating data usage")


if __name__ == '__main__':
    populate_db()
