import datetime
import random
import string


def random_text(length: int = 16) -> str:
    return "".join(random.choices(string.ascii_letters, k=length))


def random_email(length: int = 16) -> str:
    name = "".join(random.choices(string.ascii_letters, k=length))
    return f"{name}@mail.com"


def random_price() -> int:
    return random.randint(5, 50)


def random_small_price() -> float:
    return random.randint(0, 5) + round(random.random(), 4)


def random_kilobytes() -> int:
    return random.randint(1_000_000, 10_000_000)


def random_used_kilobytes() -> int:
    return random.randint(100, 10_000)


def random_seconds() -> int:
    return random.randint(1, 200)


def random_datetime():
    start_date = datetime.datetime(2021, 5, 1)
    random_days = random.randint(1, 3)

    return start_date + datetime.timedelta(days=random_days)
