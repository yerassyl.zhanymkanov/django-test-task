from rest_framework import serializers

from src.usage.models import AggregatedUsage


class AggregatedUsageSerializer(serializers.ModelSerializer):
    att_subscription = serializers.IntegerField(read_only=True)
    sprint_subscription = serializers.IntegerField(read_only=True)
    date = serializers.DateField(read_only=True)
    exceeded_data_costs = serializers.IntegerField(read_only=True)
    exceeded_voice_costs = serializers.IntegerField(read_only=True)
    total_data_costs = serializers.IntegerField(read_only=True)
    total_voice_costs = serializers.IntegerField(read_only=True)
    price_limit = serializers.IntegerField(read_only=True)
    exceeded_usage_types = serializers.ListField(read_only=True)

    class Meta:
        model = AggregatedUsage
        fields = (
            "att_subscription",
            "sprint_subscription",
            "date",
            "exceeded_data_costs",
            "exceeded_voice_costs",
            "total_data_costs",
            "total_voice_costs",
            "exceeded_usage_types",
            "price_limit",
        )


class BaseMetricsSerializer(serializers.ModelSerializer):
    att_subscription = serializers.IntegerField(read_only=True)
    sprint_subscription = serializers.IntegerField(read_only=True)

    class Meta:
        abstract = True


class DataMetricsSerializer(BaseMetricsSerializer):
    total_data_costs = serializers.DecimalField(
        read_only=True, decimal_places=2, max_digits=5
    )
    total_data_usage = serializers.IntegerField(read_only=True)

    class Meta:
        model = AggregatedUsage
        fields = (
            "att_subscription",
            "sprint_subscription",
            "total_data_costs",
            "total_data_usage",
        )


class VoiceMetricsSerializer(BaseMetricsSerializer):
    total_voice_costs = serializers.DecimalField(
        read_only=True, decimal_places=2, max_digits=5
    )
    total_voice_usage = serializers.IntegerField(read_only=True)

    class Meta:
        model = AggregatedUsage
        fields = (
            "att_subscription",
            "sprint_subscription",
            "total_voice_costs",
            "total_voice_usage",
        )
