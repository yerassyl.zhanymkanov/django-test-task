from django.contrib.postgres.fields import ArrayField
from django.db import models
from django.db.models import Sum, Q, F, Case, When, Value

from src.usage.constants import ALLOWED_USAGE_TYPES


class AggregatesManager(models.Manager):
    def price_limits(self, price_limit: int):
        return self.values(
            "att_subscription",
            "sprint_subscription",
        ).annotate(
            total_data_costs=Sum("data_price"),
            total_voice_costs=Sum("voice_price")
        ).filter(
            Q(total_data_costs__gt=price_limit) | Q(total_voice_costs__gt=price_limit)
        ).annotate(
            exceeded_data_costs=(
                Case(
                    When(
                        total_data_costs__gt=price_limit,
                        then=F("total_data_costs") - price_limit
                    ),
                    default=0
                )
            ),
            exceeded_voice_costs=(
                Case(
                    When(
                        total_voice_costs__gt=price_limit,
                        then=F("total_voice_costs") - price_limit
                    ),
                    default=0
                )
            ),
            exceeded_usage_types=Case(
                When(
                    Q(exceeded_data_costs__gt=0) & Q(exceeded_voice_costs__gt=0),
                    then=Value(["data", "voice"])
                ),
                When(Q(exceeded_data_costs__gt=0), then=Value(["data"])),
                When(Q(exceeded_voice_costs__gt=0), then=Value(["voice"])),
                output_field=ArrayField(models.CharField())
            )
        )

    def metrics(self, usage_type: str):
        subscriptions = self.values(
            "att_subscription",
            "sprint_subscription",
        )

        if usage_type == "data":
            return self._data_metrics(subscriptions)

        if usage_type == "voice":
            return self._voice_metrics(subscriptions)

        raise ValueError(f"usage-type is invalid. Must be one of {ALLOWED_USAGE_TYPES}")

    def _data_metrics(self, queryset):
        return queryset.annotate(
            total_data_costs=Sum("data_price"),
            total_data_usage=Sum("total_kilobytes"),
        )

    def _voice_metrics(self, queryset):
        return queryset.annotate(
            total_voice_costs=Sum("voice_price"),
            total_voice_usage=Sum("total_seconds"),
        )
