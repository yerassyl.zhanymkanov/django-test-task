from rest_framework.exceptions import APIException

from src.usage.constants import ALLOWED_USAGE_TYPES


class PriceLimitRequired(APIException):
    status_code = 400
    default_detail = '"price-limit" parameter is required'


class InvalidPriceLimit(APIException):
    status_code = 400
    default_detail = '"price-limit" is invalid. Must be integer'


class UsageTypeRequired(APIException):
    status_code = 400
    default_detail = '"usage-type" parameter is required'


class DatesRequired(APIException):
    status_code = 400
    default_detail = '"from-date" and "to-date" fields are required'


class InvalidDateFormat(APIException):
    status_code = 400
    default_detail = "Date format is invalid. Must be YYYY-MM-DD"


class InvalidUsageType(APIException):
    status_code = 400
    default_detail = f'Usage type is invalid. Must be one of {ALLOWED_USAGE_TYPES}'
