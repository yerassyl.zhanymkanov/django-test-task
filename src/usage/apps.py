from django.apps import AppConfig


class UsageConfig(AppConfig):
    name = 'src.usage'
    label = 'usage'
