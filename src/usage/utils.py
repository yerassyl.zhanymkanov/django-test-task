from datetime import datetime

from src.usage.exceptions import (
    InvalidUsageType, UsageTypeRequired, PriceLimitRequired, InvalidPriceLimit
)
from src.usage.constants import ALLOWED_USAGE_TYPES


def is_valid_date(raw_date: str):
    try:
        datetime.strptime(raw_date, "%Y-%m-%d").date()
    except ValueError:
        return False
    else:
        return True


def validate_usage_type(usage_type: str):
    if not usage_type:
        raise UsageTypeRequired()

    usage_type = usage_type.lower()
    if usage_type not in ALLOWED_USAGE_TYPES:
        raise InvalidUsageType()

    return usage_type


def validate_price_limit(price_limit: int):
    if not price_limit:
        raise PriceLimitRequired()

    try:
        return int(price_limit)
    except ValueError:
        raise InvalidPriceLimit()
