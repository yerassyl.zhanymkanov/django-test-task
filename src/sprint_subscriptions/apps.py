from django.apps import AppConfig


class SprintSubscriptionsConfig(AppConfig):
    name = 'src.sprint_subscriptions'
    label = 'sprint_subscriptions'
