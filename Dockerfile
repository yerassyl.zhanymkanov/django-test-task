FROM python:3.9-slim-buster

RUN apt-get update && \
    apt-get install -y gcc git libpq-dev libmagic1 && \
    apt clean && \
    rm -rf /var/cache/apt/*

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1
ENV PYTHONIOENCODING utf-8

COPY requirements.txt /tmp/

RUN pip install -U pip && \
    pip install --no-cache-dir -r /tmp/requirements.txt

COPY . /src

RUN useradd -m -d /src -s /bin/bash app \
    && chown -R app:app /src/*

WORKDIR /src
USER app